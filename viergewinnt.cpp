#include <iostream>
#include <sstream>

using namespace std;

uint64_t win_situations[] =
    {
        0b\
1111000\
0000000\
0000000\
0000000\
0000000\
0000000,

        0b\
0111100\
0000000\
0000000\
0000000\
0000000\
0000000,

        0b\
0011110\
0000000\
0000000\
0000000\
0000000\
0000000,

        0b\
0001111\
0000000\
0000000\
0000000\
0000000\
0000000,

        0b\
0000000\
1111000\
0000000\
0000000\
0000000\
0000000,

        0b\
0000000\
0111100\
0000000\
0000000\
0000000\
0000000,

        0b\
0000000\
0011110\
0000000\
0000000\
0000000\
0000000,

        0b\
0000000\
0001111\
0000000\
0000000\
0000000\
0000000,

        0b\
0000000\
0000000\
1111000\
0000000\
0000000\
0000000,

        0b\
0000000\
0000000\
0111100\
0000000\
0000000\
0000000,

        0b\
0000000\
0000000\
0011110\
0000000\
0000000\
0000000,

        0b\
0000000\
0000000\
0001111\
0000000\
0000000\
0000000,

        0b\
0000000\
0000000\
0000000\
1111000\
0000000\
0000000,

        0b\
0000000\
0000000\
0000000\
0111100\
0000000\
0000000,

        0b\
0000000\
0000000\
0000000\
0011110\
0000000\
0000000,

        0b\
0000000\
0000000\
0000000\
0001111\
0000000\
0000000,

        0b\
0000000\
0000000\
0000000\
0000000\
1111000\
0000000,

        0b\
0000000\
0000000\
0000000\
0000000\
0111100\
0000000,

        0b\
0000000\
0000000\
0000000\
0000000\
0011110\
0000000,

        0b\
0000000\
0000000\
0000000\
0000000\
0001111\
0000000,

        0b\
0000000\
0000000\
0000000\
0000000\
0000000\
1111000,

        0b\
0000000\
0000000\
0000000\
0000000\
0000000\
0111100,

        0b\
0000000\
0000000\
0000000\
0000000\
0000000\
0011110,

        0b\
0000000\
0000000\
0000000\
0000000\
0000000\
0001111,

        0b\
1000000\
1000000\
1000000\
1000000\
0000000\
0000000,

        0b\
0000000\
1000000\
1000000\
1000000\
1000000\
0000000,

        0b\
0000000\
0000000\
1000000\
1000000\
1000000\
1000000,

        0b\
0100000\
0100000\
0100000\
0100000\
0000000\
0000000,

        0b\
0000000\
0100000\
0100000\
0100000\
0100000\
0000000,

        0b\
0000000\
0000000\
0100000\
0100000\
0100000\
0100000,

        0b\
0010000\
0010000\
0010000\
0010000\
0000000\
0000000,

        0b\
0000000\
0010000\
0010000\
0010000\
0010000\
0000000,

        0b\
0000000\
0000000\
0010000\
0010000\
0010000\
0010000,

        0b\
0001000\
0001000\
0001000\
0001000\
0000000\
0000000,

        0b\
0000000\
0001000\
0001000\
0001000\
0001000\
0000000,

        0b\
0000000\
0000000\
0001000\
0001000\
0001000\
0001000,

        0b\
0000100\
0000100\
0000100\
0000100\
0000000\
0000000,

        0b\
0000000\
0000100\
0000100\
0000100\
0000100\
0000000,

        0b\
0000000\
0000000\
0000100\
0000100\
0000100\
0000100,

        0b\
0000010\
0000010\
0000010\
0000010\
0000000\
0000000,

        0b\
0000000\
0000010\
0000010\
0000010\
0000010\
0000000,

        0b\
0000000\
0000000\
0000010\
0000010\
0000010\
0000010,

        0b\
0000001\
0000001\
0000001\
0000001\
0000000\
0000000,

        0b\
0000000\
0000001\
0000001\
0000001\
0000001\
0000000,

        0b\
0000000\
0000000\
0000001\
0000001\
0000001\
0000001,

        0b\
1000000\
0100000\
0010000\
0001000\
0000000\
0000000,

        0b\
0000000\
1000000\
0100000\
0010000\
0001000\
0000000,

        0b\
0000000\
0000000\
1000000\
0100000\
0010000\
0001000,

        0b\
0100000\
0010000\
0001000\
0000100\
0000000\
0000000,

        0b\
0000000\
0100000\
0010000\
0001000\
0000100\
0000000,

        0b\
0000000\
0000000\
0100000\
0010000\
0001000\
0000100,

        0b\
0010000\
0001000\
0000100\
0000010\
0000000\
0000000,

        0b\
0000000\
0010000\
0001000\
0000100\
0000010\
0000000,

        0b\
0000000\
0000000\
0010000\
0001000\
0000100\
0000010,

        0b\
0001000\
0000100\
0000010\
0000001\
0000000\
0000000,

        0b\
0000000\
0001000\
0000100\
0000010\
0000001\
0000000,

        0b\
0000000\
0000000\
0001000\
0000100\
0000010\
0000001,

        0b\
0001000\
0010000\
0100000\
1000000\
0000000\
0000000,

        0b\
0000000\
0001000\
0010000\
0100000\
1000000\
0000000,

        0b\
0000000\
0000000\
0001000\
0010000\
0100000\
1000000,

        0b\
0000100\
0001000\
0010000\
0100000\
0000000\
0000000,

        0b\
0000000\
0000100\
0001000\
0010000\
0100000\
0000000,

        0b\
0000000\
0000000\
0000100\
0001000\
0010000\
0100000,

        0b\
0000010\
0000100\
0001000\
0010000\
0000000\
0000000,

        0b\
0000000\
0000010\
0000100\
0001000\
0010000\
0000000,

        0b\
0000000\
0000000\
0000010\
0000100\
0001000\
0010000,

        0b\
0000001\
0000010\
0000100\
0001000\
0000000\
0000000,

        0b\
0000000\
0000001\
0000010\
0000100\
0001000\
0000000,

        0b\
0000000\
0000000\
0000001\
0000010\
0000100\
0001000
};

class invalid_move_exception : exception {};

void print_board(uint64_t player_one_stones, char player_one_symbol, uint64_t player_two_stones, char player_two_symbol, char empty_field_symbol = '.')
{
  // Header with column index to guide the player
  cout << "1234567\n";
  // Strart drawing at top left corner.
  uint64_t mask = 0x20000000000ull;
  for (int i = 0; i < 6; i++)
  {
    for (int j = 0; j < 7; j++)
    {
      // stones of two players overalp; they should not
      if (mask & (player_one_stones & player_two_stones))
      {
        throw exception();
      }
      else if (mask & player_one_stones)
      {
        cout << player_one_symbol;
      }
      else if (mask & player_two_stones)
      {
        cout << player_two_symbol;
      }
      else
      {
        cout << empty_field_symbol;
      }
      mask >>= 1;
    }
    cout << endl;
  }
}

uint64_t drop_new_stone(uint64_t active_players_stones, uint64_t passive_players_stones, int column)
{
  if (column < 0 || column > 6)
  {
    throw invalid_move_exception();
  }

  uint64_t board = active_players_stones | passive_players_stones;
  
  // Start from the bottom and search for the first empty slot in the
  // column. Throw an exception is the column is full. 
  uint64_t new_stone = 0b1000000ull >> column;
  for(int i = 0; i < 6; i++)
  {
    if ((board & new_stone) == 0)
    {
      return active_players_stones | new_stone;
    }
    new_stone <<= 7;
  }
  throw invalid_move_exception();
}

bool has_won(uint64_t stones)
{
  for(uint64_t win_situation : win_situations)
  {
    if ((win_situation & stones) == win_situation)
    {
      return true;
    }
  }
  return false;
}

bool moves_are_possible(uint64_t player_one, uint64_t player_two)
{
  return (player_one | player_two) != 0x2ffffffffffull;
}

int main(int argc, char const *argv[])
{
  uint64_t active_players_stones = 0;
  uint64_t passive_players_stones = 0;
  char active_players_symbol = 'X';
  char passive_playvers_symbol = 'O';
  
  print_board(active_players_stones, active_players_symbol, passive_players_stones, passive_playvers_symbol);
  while(moves_are_possible(active_players_stones, passive_players_stones))
  {
      cout << active_players_symbol << "'s turn. Choose a column (1-7): ";
      int drop_to = -1;
      string input_line;
      getline(cin, input_line);
      stringstream(input_line) >> drop_to;
      try
      {
        active_players_stones = drop_new_stone(active_players_stones, passive_players_stones, drop_to - 1);
      }
      catch(invalid_move_exception e)
      {
        cout << "Invalid move. Try again.\n";
        continue;
      }
      if (has_won(active_players_stones))
      {
        print_board(active_players_stones, active_players_symbol, passive_players_stones, passive_playvers_symbol);
        cout << "Congratulations! " << active_players_symbol << " won.\n";
        return 0;
      }
      swap(active_players_stones, passive_players_stones);
      swap(active_players_symbol, passive_playvers_symbol);
      print_board(active_players_stones, active_players_symbol, passive_players_stones, passive_playvers_symbol);
  }
  cout << "Nice play on both sides! It's a draw.\n";
  return 0;
}
